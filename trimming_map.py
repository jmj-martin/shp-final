#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 25 11:17:30 2021

@author: juliettemartin

This program parses the 1488 files with S-curves during trimming and plots 2D maps of either TDAC or threshold
"""

import numpy as np
import glob
import time
import matplotlib.pyplot as plt

#function to collect all filenames
def collect_filenames():
    
    #there are 340 rows
    row_index = np.arange(0, 373, 1)
    glob_name = []
    
    #extract relevant data filenames
    for num in row_index:

        file = './*row' + str(row_index[num]) + '.dat'
        g = glob.glob(file)
        glob_name.append(g)
    
    #flatten list of lists
    flat_list = flatten_list(glob_name)
    
    extra = './'

    #get rid of characters in filename './'

    for i in range(len(flat_list)):
    
        for char in flat_list[i]:
        
            flat_list[i] = flat_list[i].replace(extra, '')

    return flat_list

#flatten the list
def flatten_list(big_list):
    
    flat_list = []
    
    for sublist in big_list:
        
        for item in sublist:
            
            flat_list.append(item)
    
    return flat_list

#split strings by tab
def tidy_string(string):
    
    li = list(string.split("\t")) 
    
    return li 

#extract the relevant summary data
def collect_data(filename):
    
    filename = filename
    
    
    test = []

    with open(filename, 'r') as f:
        
        for line in f:
            
            #get sumamry for selected TDAC values
            if '##' in line:    

                test.append(str(line))

    new = []

    
    #tidy everything up to have a list with items list of 4
    #list of 4 : col, row, TDAC, thresh (V)
    for j in range(len(test)):
        
        item = test[j]
        new_item = tidy_string(item)
        new.append(new_item)
        

    for k in range(len(new)):
        
        column = new[k][0]
        row =  new[k][1]
        TDAC = new[k][2]
        volt = new[k][3]
        #get rid of \n and ##
        column = column[4:]
        volt = volt[:-1]
        
        new[k][0] = float(column)
        new[k][1] = float(row)
        new[k][2] = float(TDAC)
        new[k][3] = float(volt)
        
    relevant_data = new
    

    return relevant_data

             
def plot_map(full_data):

  #contruct array of zeros to represent pixel array
    arr = np.zeros([372,132])
    
    #replace zero with TDAC or threshold
    
    for i in range(len(full_data)):
        
        col = int(full_data[i][0]) 
        row = int(full_data[i][1])
    
        tdac = full_data[i][2] #change 2 to 3 if you want threshold
        arr[row][col] = tdac #map relevant TDAC value to array of 0
        
    plt.pcolor(arr)
    #can change labels for threshold
    plt.colorbar().set_label('Optimum TDAC value') 
    plt.title('Optimum TDAC values post-tuning')
    plt.xlabel('column')
    plt.ylabel('row')
    plt.savefig('map1.png')
    
                
#run everything         
def main():
    
    start_time = time.time()    #start clock
    filenames = collect_filenames()  #get the relevant filenames
    behemoth_list = [] #will be list of lists containing data to be flattened
    
    #
    for file in range(len(filenames)):
        
        data_raw = collect_data(filenames[file])
        behemoth_list.append(data_raw)
        

    final_data = flatten_list(behemoth_list) #flatten list of lists (all data)

    plot_map(final_data) #plot TDAC or threshold map

    
    print('program runtime', "%s seconds" % (time.time() - start_time))
    
    


    
main()
