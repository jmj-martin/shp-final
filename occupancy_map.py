#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 14:09:07 2021

@author: juliettemartin

This script parses the hit data that has been written to a .dat file, and plots an occupancy map in the form of a 2D histogram
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas

def get_data(filename):
    
    #index_col set to False or won't read the .dat column 0 (corresponds to column)
    #file is tab separated
    data = pandas.read_csv(filename, sep = "\t", index_col=False)
    
    #get row and column data into arrays
    row = np.array(data['Row;'])
    column = np.array(data['# Column;'])
    
    return column, row

def plot_occupancy(column, row):
    
    #plot the 2D histogram (occupancy) and save to file
    plt.hist2d(column, row, bins = (33, 93), range = [[0, 131], [0,371]], cmap = 'viridis')
    plt.colorbar()
    plt.xlabel('Column')
    plt.ylabel('Row')
    plt.title('$^{241}$Am - 17.5 hour exposure')
    plt.savefig('occupancy_map.png')
    plt.close()

#run program
def main():
    
    column, row = get_data('Readout_241Am_HV0_17p5h_17Feb2021.dat')
    plot_occupancy(column, row)
    
main()