# SHP-final

Python scripts for analysis of ATLASPix3 data from GECCO readout. Repository is public to allow viewing for marking purposes/potential future use.

tot_distribution.py is a file that takes a local .dat file with hit data and plots a distribution of the ToT on a histogram.

occupancy_map.py is a file that takes a local .dat file with hit data and plots a hit occupancy map (2D histogram).

trimming_map.py parses 1488 trimming files plots, then colormap of optimum threshold or TDAC post-trimming of full pixel matrix (2D histogram).

s_curves_select.py partitions the pre-trimming S-curve data returned by GECCO, plots pre-trimming S-curve by TDAC value, and also the post-trimming S-curve. Can also fit an sigmoind function to a few individual curves.

initial_plot.py performs the 1D histograms of distributions of threshold and TDAC, then the 2D colourmaps showing the threshold/TDAC distributions across the pixel matrix of ATLASPix3.
