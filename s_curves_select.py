#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 16:33:58 2021

@author: juliettemartin
"""
import numpy as np
import matplotlib.pyplot as plt
import glob
import matplotlib
import scipy.optimize as opt
import re

#function to collect all filenames
def collect_filenames():
    #there are 371 rows on ATLASPix3
    row_index = np.arange(0, 372, 1)
    glob_name = []
    
    for num in row_index:
        
        file = './*row' + str(row_index[num]) + '.dat'
        g = glob.glob(file)
        glob_name.append(g)

    flat_list = []
    
    for sublist in glob_name:
        
        for item in sublist:
            
            flat_list.append(item)
    
    pesky = './'

    #get rid of pesky filename ./

    for i in range(len(flat_list)):
    
        for char in flat_list[i]:
        
            flat_list[i] = flat_list[i].replace(pesky, '')
    

    return flat_list

#split strings by tab
def tidy_string(string):
    
    li = list(string.split("\t"))    
    
    return li 

#flatten the list of sublists into one long list
def flatten_list(big_list):
    
    flat_list = []
    
    for sublist in big_list:
        
        for item in sublist:
            
            flat_list.append(item)
    
    return flat_list

#collect final trimming results from GECCO (4 columns at end of file starting with ##)
def collect_data(filename):
    
    filename = filename
    
    
    test = []

    with open(filename, 'r') as f:
        
        for line in f:
            
            #get summary for selected TDAC values
            if '##' in line:    

                test.append(str(line))

    new = []

    
    #tidy everything up to have a list with items list of 4
    #list of 4 : col, row, TDAC, thresh (V)
    for j in range(len(test)):
        
        item = test[j]
        new_item = tidy_string(item)
        new.append(new_item)
        

    for k in range(len(new)):
        
        column = new[k][0]
        row =  new[k][1]
        TDAC = new[k][2]
        volt = new[k][3]
        #get rid of \n and ##
        column = column[4:]
        volt = volt[:-1]
        
        new[k][0] = float(column)
        new[k][1] = float(row)
        new[k][2] = float(TDAC)
        new[k][3] = float(volt)
        
    relevant_data = new
    
    return relevant_data

#
def tidy(events):
    thresh = []
    sig = []
    
    for i in range(len(events)):
        
        test = tidy_string(events[i])
        test=test[:-1]
        event = float(test[i][1])
                    
        th = float(test[i][0])
        thresh.append(th)
        sig.append(event)    

#get relevant S-curve data from lines not containing some key words
#BEHOLD the most inefficient piece of garbage you ever did see
#I am scared to touch it, because it works  
#looping over a list made it throw a tantrum and idk why
#the garbage stays, for now
def read_scurves(filename):

    tdac_pix = []
    
    forbidden = ['##', 'Column','Threshold','oise','Chosen', 'inj']
    
    counter = 0

    with open(filename, 'r') as f:
        
        for line in f:
            
            if '##' not in line:
                
                if 'Column' not in line:
                    
                    if 'Threshold' not in line:
                        
                        if 'oise' not in line:
                    
                            if 'Chosen' not in line:
                                
                                if 'inj' not in line:
                                
                                    if line != '\n':
                
                                        tdac_pix.append(line)
                                    

    return tdac_pix

#this is the most disgusting list of lists of lists you ever did see
#look, I had to partition the data somehow
def split_data(tdac_pix):
    
    pix = []
    
    for i in range(len(tdac_pix)):
        
        if 'Pixel' in tdac_pix[i]:
            
            pix.append(tdac_pix[i])
    
    storage = [[] for _ in range(len(pix))]
    
    counter = -1
    
    
    for j in range(len(tdac_pix)):
        
        if 'TDAC' in tdac_pix[j]:
            
            counter += 1
            
        storage[counter].append(tdac_pix[j])
        
    
    for k in range(len(storage)):
        
        storage[k] = storage[k][1:]
        
        for l in range(len(storage[k])):
            
            item = storage[k][l]
            storage[k][l] = item[:-1]

            li = tidy_string(storage[k][l])
            
            storage[k][l] = li
            
    for a in range(len(storage)):
        
        for b in range(len(storage[a])):
            
            for c in range(len(storage[a][b])):
                
                storage[a][b][c] = float(storage[a][b][c])
                
    return storage, pix

#plot S-curves at a given TDAC defined in this function
def select_tdac(storage, pix):
    
    tdac = 7
    
    lookup = 'for TDAC=' + str(tdac)
    
    x = []
    y = []
    
    for i in range(len(pix)):
        
        if lookup in pix[i]:
            
            dataset = storage[i]
            
            for j in range(len(dataset)):
                
                x.append(dataset[j][0])
                y.append(dataset[j][1])
                
    counter = 0      
    for k in range(len(y)):
        
        if y[k] > 100:
            
            counter += 1
            
    percentage = (counter/len(y))*100
    
    print('TDAC=' + str(tdac))
    
    print(len(x), '<-- number of entries')
    print('number of entries overshot = ', counter)
    print('this is ' + str(percentage) + '% of entries')
    
    #plot         
    plt.hist2d(x, y, bins = (25,55), cmap = 'viridis', norm=matplotlib.colors.LogNorm())
    plt.colorbar()
    plt.title('S-curves for TDAC=' + str(tdac))
    plt.xlabel('Voltage (V)')
    plt.ylabel('number of signals detected (of 100)')

#plot S-curve at optimum TDAC for each pixel (2D histogram)
#quantify overshoot, if applicable
def plot_optimum(optimum, storage):
    
    x = []
    y = []
    counter = 0
    
    for i in range(len(optimum)):
        
        index = optimum[i]
        s_curve = storage[index]
        
        for j in range(len(s_curve)):
            
            sig = s_curve[j][1]
            x.append(s_curve[j][0])
            y.append(sig)
            
            #check overshoot, can change to max. number of injections
            if sig > 100:
                
                counter += 1
    
    #plot
    plt.hist2d(x, y, bins = (25,55), cmap = 'viridis', norm=matplotlib.colors.LogNorm())
    plt.colorbar()
    plt.title('S-curves for optimised TDAC')
    plt.xlabel('Voltage (V)')
    plt.ylabel('number of signals detected (of 100)')
    
    #quantify overshoot, if any
    percentage = (counter/len(y))*100
    print('percentage of overshoot values:', percentage)

#sigmoid function for fitting
def f(x, a, b, c, d):
    
    y = a / (1. + np.exp(-c * (x - d))) + b
    return y     

#fit sigmoid function to individual S-curves
def fit_scurve(data, identifier):
    
    x = []
    y = []
    
    for i in range(len(data)):
        
        x.append(data[i][0])
        y.append(data[i][1])
    

    s = identifier
    result = re.search(r'\((.*?)\)',s).group(1)
    
    #perform sigmoid fit to S-curve data
    (a_, b_, c_, d_), _ = opt.curve_fit(f, x, y, bounds = (0, [150, 0.001, 150, 0.7]))
    label = 'Pixel '+ '(' + result + '), ' + 'TDAC=' + str(identifier[-2])
    y_model = f(x, a_, b_, c_, d_) #model based on fitted parameters
    print(a_, b_, c_, d_) #print parameters of fit
    
    #plot
    plt.plot(x, y, 'o',  label = label)
    plt.plot(x, y_model, '--k')
    plt.legend(loc = 'best')
    plt.xlabel('Injected voltage (V)')
    plt.ylabel('Signals detected (of 100)')
    plt.title('SCurves for selected pixels')  
    plt.savefig('indiv-pixels.png')
    plt.close()
    
    #interpolate for rough calculation of threshold (not exact)
    thresh = np.interp(50, y, x)
    print(identifier +'threshold:'+ str(thresh) + '\n')
            
            
#run everything
def main():
    
    filenames = collect_filenames()
    big_data = []
    
    for i in range(len(filenames)):

        tdac_pix = read_scurves(filenames[i])
        big_data.append(tdac_pix)
    

    big_data = flatten_list(big_data)

    storage, pix = split_data(big_data)
    print('\n storage ready')
    #select_tdac(storage, pix)
    
    behemoth_list = [] #will be list of lists containing data to be flattened
    
    #collect raw summary data by line
    for file in range(len(filenames)):
        
        data_raw = collect_data(filenames[file])
        behemoth_list.append(data_raw)
        
    col = []
    row = []
    tdac = []
        
    
    final_data = flatten_list(behemoth_list) #flatten list of lists 
    
    #get row, column, TDAC
    for j in range(len(final_data)):
        
        col.append(final_data[j][0])
        row.append(final_data[j][1])
        tdac.append(final_data[j][2])
    
    #array for faster processing
    col = np.array(col)
    row = np.array(row)
    tdac = np.array(tdac)
    print('\n data compiled')
    
    keys = []
    
    #create lookup from GECCO returned optimised values
    for k in range(len(tdac)):
        
        lookup = '# SCurve: Pixel (' + str(int(col[k])) +'|' +str(int(row[k])) +')'  + ' for TDAC=' + str(int(tdac[k])) +'\n'
        keys.append(lookup)

    print('\n lookup ready')
    
    #get indices of optimal pixel + tdac values for plotting
    optimum = [pix.index(l) for l in keys]
    
    print('\n indices identified')
    print('\n length of indices', len(optimum))

    
    #plot S-curves for each pixel at optimum TDAC
    plot_optimum(optimum, storage)
    
    #select some individual pixels to plot
    random = [1, 5, 10, 14]
    
    for item in range(len(random)):
        
        index = random[item]
        
        #fit s-curve and return threshold for individual S-curves
        fit_scurve(storage[index], pix[index])
    

main()
