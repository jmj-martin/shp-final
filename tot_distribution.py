#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 14:24:36 2021

@author: juliettemartin

This script parses the hit data that has been written to a .dat file, and plots the ToT distribution
TS1 = signal rising edge
TS2 = signal falling edge
Plots TS1, TS2
"""

import pandas
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

#parse file and get data for ToT
def get_tot(filename):
    
    #index_col omitted or else won't read final column (ToT) for some reason
    #file is tab separated
    data = pandas.read_csv(filename, sep = "\t")
    
    #get tot values in form ToT --> x
    tot = np.array(data['ext. ToT'])
    tot_raw = []
    
    #need to remove all string characters from ToT
    #so the 'ToT-->' bit, leaving only numbers
    for j in range(len(tot)):
    
        str = tot[j]
        item = [int(s) for s in str.split() if s.isdigit()]
        tot_raw.append(item)
        
    #list needs flattened in order to be ready to plot
    tot_plot = [val for sublist in tot_raw for val in sublist]   
    
    return tot_plot

#function to plot signal falling edge timestamp, TS2  
def plot_TS2(filename):
    
    dat = pandas.read_csv(filename, sep = "\t", index_col=False)
    TS2 = dat['ToT;'] #this is relabeled TS2
    plt.hist(TS2, bins = 128, histtype='step')
    plt.xlabel('TS2')
    plt.ylabel('Counts')
    plt.title('cosmic muon approx 48h exposure')
    plt.show()
    #plt.savefig('TS2_distrib_source2_195.png')
    #plt.close()

#plot ToT distribution and save to file
def plot_tot(tot):
    
    plt.hist(tot, bins = 127, histtype='step')
    plt.xlabel('ToT')
    plt.ylabel('Counts')
    plt.yscale('log')
    plt.title('cosmic muon - approx. 48 hour exposure')
    plt.savefig('ToT_distrib.png')
    plt.close()
    
#function to plot signal rising edge timestamp, TS1    
def plot_TS1(filename):
    
    dat = pandas.read_csv(filename, sep = "\t", index_col=False)
    TS1 = dat['Timestamp;'] #this is relabeled TS1
    plt.hist(TS1, range = (-0.5, 1023.5), bins = 64, histtype='step')
    plt.xlabel('TS1')
    plt.ylabel('Counts')
    plt.title('Cosmic muon, approx. 48h exposure')
    plt.savefig('TS1_distrib_source2_195.png')
    plt.close()
    

#run everything    
def main():
    
    filename = 'Readout_cosmic.dat'
    tot = get_tot(filename)
    plot_tot(tot)
    plot_TS2(filename)
    plot_TS1(filename)
    
main()
    